import express from 'express';
import { Request, Response } from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';

const app = express();
const server = http.createServer(app);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get(
    "/",
    async (req: Request, res: Response): Promise<Response> => {
        return res.status(200).send({
            message: "Hello World!",
        });
    }
);

const wss = new WebSocket.Server({ noServer: true });

const wsClients: Map<string, WebSocket[]> = new Map();

wss.on('connection', (ws: WebSocket, req: Request) => {

    console.log("req url = " + req.url);
    const id = req.url.substring(1);
    console.log("id = " + id);
    let sockets = wsClients.get(id);
    if (!sockets) {
        sockets = [];
        console.log("adding empty sockets array");
        wsClients.set(id, sockets);
    }
    console.log("adding socket to array");
    sockets.push(ws)
    ws.on('close', () => {
        console.log("socket closed: ");
        let sockets = wsClients.get(id);
        if (sockets) {
            var index = sockets.indexOf(ws);
            if (index !== -1) {
                console.log("removing socket from array");
                sockets.splice(index, 1);
            }
        }
    })
    ws.on('error', (err) => {
        console.log('ws error: ', err);
    })
});

type Notification = {
    type: string[],
    object: {
        id: any,
    },
}

app.post('/webhook/:id', (req: Request, res: Response) => {
    const notification = req.body;
    console.log(notification);

    // send the incoming notification to the connected websocket clients
    const clients = wsClients.get(req.params.id);
    if (clients) {
        clients.forEach((client) => client.send(JSON.stringify(notification, undefined, 2)))
    }
    res.sendStatus(200);
});

server.on('upgrade', async(req, socket, head) => {
    try {
      wss.handleUpgrade(req, socket, head, (ws) => {
        wss.emit('connection', ws, req)
      })
    } catch(err) {
      console.log('Socket upgrade failed', err)
      socket.destroy()
    }
});

server.listen(process.env.PORT || 8000, () => {
    console.log(`Server started on port 8000 :)`);
});
